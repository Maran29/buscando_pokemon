package com.example.grupo_gregorio.pokemongo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class PokemonesEncontrados extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_lista_pokemon);
        ListView lv = (ListView) findViewById(R.id.lvlista);
        TextView poke_encontrado = (TextView) findViewById(R.id.text_encontrado);
        ArrayList<Pokemon> itemsCompra = obtenerItems();
        PokemonAdapter adapter = new PokemonAdapter(this, itemsCompra);
        lv.setAdapter(adapter);
        poke_encontrado.setText(PokemonDB.ADIVINADOS+" "+getResources().getString(R.string.msg_adivinados));

    }

    private ArrayList<Pokemon> obtenerItems() {

        ArrayList<Pokemon> items = new ArrayList<>();
        if (PokemonDB.ADIVINADOS!=0) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.msg_capturados), Toast.LENGTH_LONG).show();

            PokemonDB.cargarDatos(getApplicationContext());
            for (int i = 0; i < PokemonDB.getTamaño(); i++) {
                boolean bol = PokemonDB.isAdivinado(i);
                if (bol) {
                    items.add(PokemonDB.getPokemon(i));
                }
            }

        }else{

        }
        return items;
    }
}
