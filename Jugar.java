package com.example.grupo_gregorio.pokemongo;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.ExecutionException;

public class Jugar extends AppCompatActivity implements View.OnClickListener {
    ImageView imagen;
    TextView lblintentos, lbladivinados, lblnivel;
    TextView lblcuenta;
    private MediaPlayer reproductor;
    private final int TIEMPO_ESPERA = 1500;
    private Button btn1, btn2, btn3, btn4;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_jugar);
        iniciarComponentes();
        PokemonDB.cargarDatos(getApplicationContext());
        reproductor = MediaPlayer.create(this, R.raw.atrapalosya);
        reproductor.setLooping(true);
        reproductor.start();
        new RestaurarTareas().execute();
        PokemonDB.cargarConfiguracion(getApplicationContext());
        marcador();


    }
    public void marcador(){
       int[] niveles = {0,4,8, 12, 16,20,24,28,32,36,40,44,48,52,56,60,64,68,72,76,80,84,88,90,94,96,100};
        if (niveles[PokemonDB.NIVEL]==PokemonDB.ADIVINADOS){
            PokemonDB.NIVEL++;
            PokemonDB.MensajeAlerta(this,getResources().getString(R.string.Felicidades), "El Nivel "+ (PokemonDB.NIVEL)+ " a sido superado sigue adelante!!");
        }
        lblintentos.setText(String.valueOf(PokemonDB.INTENTOS));
        lbladivinados.setText(String.valueOf(PokemonDB.ADIVINADOS));
        lblnivel.setText(String.valueOf(PokemonDB.NIVEL));
    }

    public void setSombra(int id) {
        int resId = getResources().getIdentifier(PokemonDB.getSombra(id), "drawable", getPackageName());
        imagen.setImageResource(resId);
    }

    public void setPokemon(int id) {
        int resId = getResources().getIdentifier(PokemonDB.getNombre(id), "drawable", getPackageName());
        imagen.setImageResource(resId);
    }

    private void iniciarComponentes() {
        imagen = (ImageView) findViewById(R.id.miimagen);
        lblintentos = (TextView) findViewById(R.id.lblintentos);
        lbladivinados = (TextView) findViewById(R.id.lbladivinados);
        lblnivel = (TextView) findViewById(R.id.lblnivel);
        lblcuenta = (TextView) findViewById(R.id.lblcuenta);
        btn1 = (Button) findViewById(R.id.btn1);
        btn2 = (Button) findViewById(R.id.btn2);
        btn3 = (Button) findViewById(R.id.btn3);
        btn4 = (Button) findViewById(R.id.btn4);
        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        btn4.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
try {
    Button boton = (Button) v;
    String nombrePokemon = boton.getText().toString().toLowerCase();
    if (PokemonDB.isPokemon(nombrePokemon)) {
        setPokemon(PokemonDB.NUMEROGENERADO);
        PokemonDB.setAdivinado(PokemonDB.NUMEROGENERADO, true);
        PokemonDB.ADIVINADOS++;
        marcador();
        habilitarBotones(false);
        boton.setVisibility(View.VISIBLE);
        boton.setClickable(false);
        esperar();
        Principal.juegocomenzado();

    } else {
        PokemonDB.DisminuirIntentos();
        lblintentos.setText(String.valueOf(PokemonDB.INTENTOS));

        v.setVisibility(View.INVISIBLE);
    }

    if (PokemonDB.isGameOver()) {
        PokemonDB.ReseterJuego(getApplicationContext());
        Principal.juegocomenzado();
        Intent i = new Intent(Jugar.this, Perder.class);
        startActivity(i);
        finish();

    }

}catch (Exception e){
    e.printStackTrace();
}

    }

    public void esperar() {
        new CountDownTimer(TIEMPO_ESPERA, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {
                lblcuenta.setText("Generando en " + (millisUntilFinished / 1000));
            }

            @Override
            public void onFinish() {
                try {
                    lblcuenta.setText("");
                    if (!PokemonDB.isWin()) {
                        new RestaurarTareas().execute();
                    } else {
                        Ganador();
                        finish();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }.start();
    }
    private void Ganador(){
        PokemonDB.MensajeAlerta(this,getResources().getString(R.string.Felicidades), getResources().getString(R.string.msg_ganaste));
    }

    public void habilitarBotones(boolean valor) {
        if (valor) {
            btn1.setVisibility(View.VISIBLE);
            btn2.setVisibility(View.VISIBLE);
            btn3.setVisibility(View.VISIBLE);
            btn4.setVisibility(View.VISIBLE);
            btn1.setClickable(true);
            btn2.setClickable(true);
            btn3.setClickable(true);
            btn4.setClickable(true);
        } else {
            btn1.setVisibility(View.INVISIBLE);
            btn2.setVisibility(View.INVISIBLE);
            btn3.setVisibility(View.INVISIBLE);
            btn4.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (PokemonDB.isGameOver()) {
            PokemonDB.removerDatos(getApplicationContext());
        } else {
            PokemonDB.guardarDatos(getApplicationContext());
        }
        reproductor.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        reproductor.start();
    }

    @Override
    protected void onDestroy() {
        if (reproductor.isPlaying()) {
            reproductor.stop();
            reproductor.release();
        }
        super.onDestroy();
    }

    private class RestaurarTareas extends AsyncTask<Void, Void, Void> {
        private ProgressDialog dialog;
        private int numero = 0;
        private int totalgenerados = 4;
        private int numerosrestantes = totalgenerados - 1;
        private int contador = 0;
        private int permitidos = 0;
        private int valorgenerado = -1;
        ArrayList<Integer> numeros = new ArrayList<>();

        @Override
        protected void onPreExecute() {
            try {
                dialog = new ProgressDialog(Jugar.this);
                dialog.setMessage("Generando ...");
                dialog.show();
            }catch (Exception e){
                e.printStackTrace();
            }


        }

        @Override
        protected Void doInBackground(Void... params) {
            do {
                numero = ((int) (Math.random() * PokemonDB.getTamaño()));
                if (!PokemonDB.isAdivinado(numero) && valorgenerado <= 0) {
                    valorgenerado = numero;
                    contador++;
                    numeros.add(numero);
                } else if (!numeros.contains(numero) && permitidos < numerosrestantes) {
                    numeros.add(numero);
                    contador++;
                    permitidos++;
                }
            } while (contador < totalgenerados);
            Collections.shuffle(numeros);

            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            try {
                PokemonDB.NUMEROGENERADO = valorgenerado;
                setSombra(valorgenerado);
                habilitarBotones(true);
                btn1.setText(PokemonDB.getNombre(numeros.get(0)));
                btn2.setText(PokemonDB.getNombre(numeros.get(1)));
                btn3.setText(PokemonDB.getNombre(numeros.get(2)));
                btn4.setText(PokemonDB.getNombre(numeros.get(3)));
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                super.onPostExecute(aVoid);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }


    }

}
