package com.example.grupo_gregorio.pokemongo;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;


public class Principal extends AppCompatActivity {
    private static Button continuar;
    private Button creditos;
    private Button listapokemon;
    private Button salir;
    private static Button juegonuevo;
    private MediaPlayer reproductor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_principal);
        continuar = (Button) findViewById(R.id.btncontinuar);
        juegonuevo = (Button) findViewById(R.id.btnjuegonuevo);
        creditos = (Button) findViewById(R.id.btnacerca);
        listapokemon = (Button) findViewById(R.id.btnlista);
        salir = (Button) findViewById(R.id.btnsalir);
        PokemonDB.cargarDatos(getApplicationContext());
        reproductor = MediaPlayer.create(this, R.raw.musicafondo);
        reproductor.setLooping(true);
        reproductor.start();
        juegocomenzado();
        continuar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (PokemonDB.isWin()) {
                    new AlertDialog.Builder(v.getContext())
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setTitle(getResources().getString(R.string.Felicidades))
                            .setMessage(getResources().getString(R.string.msg_ganaste))
                            .setNegativeButton("Cancelar", null)
                            .setPositiveButton("Juego Nuevo", new DialogInterface.OnClickListener() {// un listener que al pulsar, cierre la aplicacion
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    PokemonDB.ReseterJuego(getApplication());
                                    juegocomenzado();
                                    Intent intent = new Intent(Principal.this, Jugar.class);
                                    startActivity(intent);
                                }
                            })
                            .show();

                } else {
                    Intent intent = new Intent(Principal.this, Jugar.class);
                    startActivity(intent);
                }

            }
        });

        juegonuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (PokemonDB.ADIVINADOS!=0){
                    new AlertDialog.Builder(v.getContext())
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setTitle("Juego Nuevo")
                            .setMessage(getResources().getText(R.string.msg_reiniciar))
                            .setNegativeButton("Cancelar",null)
                            .setPositiveButton("Iniciar", new DialogInterface.OnClickListener() {// un listener que al pulsar, cierre la aplicacion
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    PokemonDB.ReseterJuego(getApplication());
                                    juegocomenzado();
                                    Intent intent = new Intent(Principal.this, Jugar.class);
                                    startActivity(intent);
                                }
                            })
                            .show();
                }else{
                    Intent intent = new Intent(Principal.this, Jugar.class);
                    startActivity(intent);
                }
            }
        });

        listapokemon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent nuevoform = new Intent(Principal.this, Pokedex.class);
                startActivity(nuevoform);
            }
        });

        creditos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent nuevoform = new Intent(Principal.this, Creditos.class);
                startActivity(nuevoform);

            }
        });

        salir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
public static void juegocomenzado(){
   if ( PokemonDB.ADIVINADOS<=0){
       continuar.setVisibility(View.INVISIBLE);
    }else{
       continuar.setVisibility(View.VISIBLE);

   }
}
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (reproductor.isPlaying()) {
            reproductor.stop();
            reproductor.release();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        reproductor.start();

    }

    @Override
    protected void onPause() {
        super.onPause();
        reproductor.pause();


    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Salir")
                    .setMessage(getResources().getText(R.string.msg_salir))

                    .setNegativeButton("Salir",new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .setPositiveButton("Verificar", new DialogInterface.OnClickListener() {// un listener que al pulsar, cierre la aplicacion
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent nuevoform = new Intent(Principal.this, PokemonesEncontrados.class);
                            startActivity(nuevoform);
                        }
                    })
                    .show();

        }

        return super.onKeyDown(keyCode, event);
    }

}
